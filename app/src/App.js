import React from 'react';
import { AuthorizeModal } from './components/AuthorizeModal';

import 'materialize-css/dist/js/materialize.js';
import 'materialize-css/dist/css/materialize.css';

function App() {
  return (
    <div>
      <header />
      <AuthorizeModal />
    </div>
  );
}

export default App;
