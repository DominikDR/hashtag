import React, { useState } from 'react';
import { Modal, Button, Tab, Tabs } from 'react-materialize';
import { Login } from './components/Login';
import { Register } from './components/Register';

const TabsMap = {
  loginTab: 'LOGIN_TAB',
  registerTab: 'REGISTER_TAB',
};

const AuthorizeModal = () => {
  const [selectedTab, setSelectedTab] = useState(TabsMap.loginTab)

  return (
    <Modal header="Modal Header" trigger={<Button>Log in</Button>}>
    <Tabs onChange={setSelectedTab}>
      <Tab title="Log in" idx={TabsMap.loginTab} active={selectedTab === Tabs.loginTab}>
        <Login />
      </Tab>
      <Tab title="Register" idx={TabsMap.registerTab} active={selectedTab === Tabs.registerTab}>
        <Register />
      </Tab>
    </Tabs>
    </Modal> 
  )
};


export default AuthorizeModal;
