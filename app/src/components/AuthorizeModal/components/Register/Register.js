import React, { useState }  from 'react';
import { TextInput, Button } from 'react-materialize';
import styled from 'styled-components';

const StyledDiv = styled.div`
  margin-top: 4px;
`

const Register = () => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async () => {
    try {
      const response = await fetch(
        'http://localhost:8000/register',
        {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          method: 'POST',
          body: JSON.stringify({ login, password })
        }
      )
      console.log(response)
    } catch(e) {
      console.error(e) 
    }
  }

  return (
    <StyledDiv>
      <TextInput label="Login" onChange={(e) => { setLogin(e.target.value); }} />
      <TextInput password label="Password" onChange={(e) => { setPassword(e.target.value); }} />
      <Button onClick={handleSubmit}>Submit</Button>
    </StyledDiv>
  );
};

export default Register;
