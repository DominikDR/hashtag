const Koa = require('koa')
const logger = require('koa-morgan')
const Router = require('koa-router')
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
import { login } from './databaseActions'


const response = {
  sth: 'sth' 
}

const router = new Router()
const server = new Koa()

router.get('/login', async (ctx) => {
  await login();
  ctx.body = response
})

router.post('/register', async (ctx) => {
  console.log(ctx.request.body)
  ctx.body = response
})

server
  .use(cors({
    origin: "*" 
  }))
  .use(logger('tiny'))
  .use(bodyParser())
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(8000, () => {
    console.log(`Server started and running on localhost:8000`)
  })

server.on('error', (err, ctx) => {
  console.error(err, ctx)
})
