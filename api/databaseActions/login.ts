import { connectToDatabase } from './generic'

export const login = async () => (
  new Promise(async (resolve, reject) => {
    const { db, client } = await connectToDatabase()
    try {
      console.log('yay!')
      resolve({ sth: 'sth' })

    } catch (e) {
      console.error(e)
      reject(e);
    } finally {
      if (client) {
        client.close()
      }
    }
  })
)
