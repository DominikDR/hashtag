import { MongoClient } from 'mongodb'

const url = 'mongodb://hashtag_mongodb:27017'
const dbName = 'hashtag'

export const connectToDatabase = async () => {
  try {
    const client = await MongoClient.connect(url, { useNewUrlParser: true })
    const db = client.db(dbName)
    return { client, db }
  } catch (e) {
    console.error(e)
    throw e;
  }
}

